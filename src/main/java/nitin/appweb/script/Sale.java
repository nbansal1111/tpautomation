package nitin.appweb.script;

import lombok.Data;

@Data
public class Sale {
	private String sr;
	private String plan;
	private String invType;
	private String invParticular;
	private String invoiceDate;
	private String amount;
}
