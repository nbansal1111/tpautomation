package nitin.appweb.script;

import java.util.List;

import lombok.Data;

@Data
public class UserInfo {
	private String clientId;
	private String username;
	private String balance;
	private List<Sale> sales = null;
	private List<PaymentReceived> paymentReceived = null;
	
	@Override
	public String toString() {
		return "UserInfo [clientId=" + clientId + ", username=" + username + ", balance=" + balance + ", sales=" + sales
				+ ", paymentReceived=" + paymentReceived + "]";
	}
	
	
}
