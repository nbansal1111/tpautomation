package nitin.appweb.script;

import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.google.gson.Gson;

public class Findnames {

	Browser br = new Browser();

	@Test
	public void getDetails() {
		try {
			WebDriver driver = br.callBrowser();
			WebDriverWait wait = new WebDriverWait(driver, 20);
			login(driver);
			Boolean isLedgerDisplayed = false;
			List<UserInfo> userInfos = new ArrayList<>();
			int[] arr = { 192593, 192593, 81545, 81545, 228877, 228877, 186618, 186618, 188775, 188775, 29657, 29657,
					186815, 186815, 205780, 205780, 193921, 193921, 30677, 30677, 188471, 188471, 183113, 183113,
					193258, 193258, 204964, 204964, 204704, 204704, 163403, 163403, 205307, 205307, 186496, 186496,
					188335, 188335, 193909, 193909, 190503, 190503, 152277, 152277, 193037, 193037, 186498, 186498,
					35353, 35353, 191575, 191575, 185138, 185138, 188728, 188728, 48455, 48455, 32486, 32486, 205078,
					205078, 189165, 189165, 158394, 158394, 193034, 193034, 184755, 184755, 193286, 193286, 188817,
					188817, 190770, 190770, 188475, 188475, 191990, 191990, 193694, 193694, 225587, 225587, 190631,
					190631, 205719, 205719, 157309, 157309, 193139, 193139, 185136, 185136, 52074, 52074, 186514,
					186514, 205916, 205916, 205561, 205561, 56723, 56723, 192549, 192549, 207, 207, 189925, 189925,
					189721, 189721, 193380, 193380, 192141, 192141, 187245, 187245, 183647, 183647, 187385, 187385,
					32991, 32991, 193883, 193883, 192459, 192459, 204439, 204439, 186833, 186833, 191501, 191501,
					190247, 190247, 204744, 204744, 189942, 189942, 78844, 78844, 190458, 190458, 191489, 191489,
					204748, 204748, 29740, 29740, 184529, 184529, 192467, 192467, 194148, 194148, 192973, 192973,
					185137, 185137, 2904, 2904, 193877, 193877, 190310, 190310, 190163, 190163, 100497, 100497, 186657,
					186657, 81449, 81449, 643, 643, 225645, 225645, 190961, 190961, 191325, 191325, 161573, 161573,
					189648, 189648, 183133, 183133, 192312, 192312, 194096, 194096, 188303, 188303, 187700, 187700,
					183655, 183655, 205037, 205037, 83308, 83308, 191333, 191333, 189130, 189130, 1323, 1323, 191294,
					191294, 189321, 189321, 194112, 194112, 193399, 193399, 185135, 185135, 190731, 190731, 205180,
					205180, 192548, 192548, 55105, 55105, 191245, 191245, 62707, 62707, 205612, 205612, 192305, 192305,
					49088, 49088, 205356, 205356, 184429, 184429, 54607, 54607, 184131, 184131, 205179, 205179, 35094,
					35094, 205965, 205965, 56351, 56351, 186179, 186179, 189162, 189162, 165943, 165943, 133469, 133469,
					133469, 133469, 71766, 71766, 34304, 34304, 47185, 47185, 431, 431, 31372, 31372, 46251, 46251,
					55052, 55052, 165364, 165364, 4708, 4708, 32107, 32107, 33810, 33810, 4767, 4767, 132993, 132993,
					153734, 153734, 60440, 60440, 5237, 5237, 53276, 53276, 33809, 33809, 110727, 110727, 31542, 31542,
					56670, 56670, 190974, 190974, 186837, 186837, 190217, 190217, 49212, 49212, 194314, 194314, 32011,
					32011, 35862, 35862, 35285, 35285, 206914, 206914, 191992, 191992, 177354, 177354, 80903, 80903,
					4002, 4002, 67306, 67306, 107709, 107709, 32012, 32012, 74641, 74641, 4223, 4223, 46696, 46696,
					62760, 62760, 62760, 192455, 192455, 33555, 33555, 184024, 184024, 187796, 187796, 53399, 53399,
					82974, 82974, 150851, 150851, 67575, 67575, 6244, 6244, 118, 118, 185128, 185128, 2021, 2021, 824,
					824, 205438, 205438, 32920, 32920, 224489, 224489, 49559, 49559, 183018, 183018, 2823, 2823, 190888,
					190888, 185197, 185197, 192411, 192411, 169674, 169674, 52002, 52002, 27923, 27923, 48362, 48362,
					205867, 205867, 30329, 30329, 4252, 4252, 84331, 84331, 192683, 192683, 54984, 54984, 182453,
					182453, 52563, 52563, 32946, 32946, 52294, 52294, 190057, 190057, 184419, 184419, 343, 343, 155495,
					155495, 54140, 54140, 49066, 49066, 20829, 20829, 54892, 54892, 109790, 109790, 68226, 68226,
					189514, 189514, 205947, 205947, 183954, 183954, 162860, 162860, 192321, 192321, 133290, 133290,
					184140, 184140, 185191, 185191, 21215, 21215, 31095, 31095, 185428, 185428, 192708, 192708, 192413,
					192413, 184629, 184629, 840, 840, 188101, 188101, 182915, 182915, 56079, 56079, 3782, 3782 };
			for (int i = 0; i < arr.length; i++) {
				driver.navigate().to("http://partner.tripleplay.in/Partner/AccountView.aspx?Id=" + arr[i]);
				try {
					isLedgerDisplayed = driver.findElement(By.xpath("(//*[@id='ContentPlaceHolder1'])[13]"))
							.isDisplayed();
				} catch (Exception e) {
					isLedgerDisplayed = false;
				}

				if (isLedgerDisplayed.equals(true)) {

					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//*[@id='ContentPlaceHolder1'])[13]")))
							.click();

					wait.until(
							ExpectedConditions.elementToBeClickable(By.xpath("(//*[@id='ContentPlaceHolder1'])[14]")))
							.click();

					Thread.sleep(3000);

					// code
					List<WebElement> CustomerDetailsHeader = driver
							.findElements(By.xpath("//*[@id='ContentPlaceHolder1_gvPurchase']/tbody/tr/th"));
					System.out.println(CustomerDetailsHeader);
					List<WebElement> CustomerDetailsValues = driver
							.findElements(By.xpath("//*[@id='ContentPlaceHolder1_gvPurchase']/tbody/tr[2]/td"));
					System.out.println(CustomerDetailsValues);

					// Map<String, String> customername = new
					// LinkedHashMap<String, String>();
					// customername.put("Client ID",
					// driver.findElement(By.id("ContentPlaceHolder1_lbl_AccNum")).getText());
					// customername.put("User Name",
					// driver.findElement(By.id("ContentPlaceHolder1_lbl_UserName")).getText());
					// customername.put("Name",
					// driver.findElement(By.id("ContentPlaceHolder1_lbl_Name")).getText());

					UserInfo user = toUser(driver.findElement(By.id("ContentPlaceHolder1_lbl_AccNum")).getText(),
							driver.findElement(By.id("ContentPlaceHolder1_lbl_UserName")).getText(),
							driver.findElement(By.id("ContentPlaceHolder1_lbl_Name")).getText());

					// System.out.println("Customer details are: " + counter +
					// ": " + customername);
					// writer.write("Customer details are: " + counter + ": " +
					// customername + "\r\n ");

					// List<String> SalesDetailsList = new ArrayList<String>();
					// List<String> PaymentReceivedList = new
					// ArrayList<String>();

					List<Sale> sales = new ArrayList<>();
					for (int k = 2; k <= driver
							.findElements(By.xpath("//*[@id='ContentPlaceHolder1_gvPurchase']/tbody/tr")).size(); k++) {

						// SalesDetailsList.add(driver
						// .findElement(By.xpath("//*[@id='ContentPlaceHolder1_gvPurchase']/tbody/tr["
						// + k + "]"))
						// .getText());

						String text = driver
								.findElement(By.xpath("//*[@id='ContentPlaceHolder1_gvPurchase']/tbody/tr[" + k + "]"))
								.getText();
						String[] split = text.split(" ");

						sales.add(createSale(split));

					}
					user.setSales(sales);

					// System.out.println(SalesDetailsList);
					// writer.write(SalesDetailsList.toString() + "\r\n");
					// System.out.println();
					// System.out.println("payment Received Details:");
					// writer.write("payment Received Details:" + "\r\n");

					List<PaymentReceived> paymentReceived = new ArrayList<>();
					for (int j = 2; j <= driver
							.findElements(By.xpath("//*[@id='ContentPlaceHolder1_gdpayment']/tbody/tr")).size(); j++) {
						// PaymentReceivedList.add(driver
						// .findElement(By.xpath("//*[@id='ContentPlaceHolder1_gdpayment']/tbody/tr["
						// + j + "]"))
						// .getText());
						String text = driver
								.findElement(By.xpath("//*[@id='ContentPlaceHolder1_gdpayment']/tbody/tr[" + j + "]"))
								.getText();

						String[] split = text.split(" ");
						PaymentReceived createPaymentDetails = createPaymentDetails(split);
						paymentReceived.add(createPaymentDetails);
					}
					user.setPaymentReceived(paymentReceived);

					userInfos.add(user);
					try (Writer writer = new FileWriter("Output.json")) {
						Gson gson = new Gson();
						gson.toJson(userInfos, writer);
						System.out.println("writing has been done");
					}

					// System.out.println(PaymentReceivedList);
					// writer.write(PaymentReceivedList.toString() + "\r\n");

					// System.out.println("******************************************************************");
					// writer.write("******************************************************************"
					// + "\r\n");
					// System.out.println();
				} else {
					// writer.write("User information is not diaplyed" +
					// "\r\n");
				}

				// writer.close();
			}

			// Gson gson = new GsonBuilder().create();
			// gson.toJson(userInfos, write);
			// System.out.println("done");

		} catch (Exception e) {
			System.out.println("Exception occur" + e.getMessage());
		}
	}

	private PaymentReceived createPaymentDetails(String[] words) {
		PaymentReceived received = new PaymentReceived();
		if (words != null) {
			received.setSr(words[0]);
			received.setRecptNo(words[1]);
			received.setDate(words[2]);
			received.setPayMethod(words[words.length - 2]);

			received.setAmount(words[words.length - 1]);
		}

		return received;

	}

	private Sale createSale(String[] words) {
		Sale sale = new Sale();
		int length = words.length;
		sale.setSr(words[0]);
		sale.setPlan(words[1]);
		sale.setInvType(words[2]);
		// sale.setInvParticular(words[3]);
		sale.setInvoiceDate(words[length - 2]);
		sale.setAmount(words[length - 1]);
		return sale;
	}

	private UserInfo toUser(String clientId, String username, String name) {
		UserInfo info = new UserInfo();
		info.setClientId(clientId);
		info.setUsername(username);
		return info;

	}

	private void login(WebDriver driver) {
		driver.manage().window().maximize();
		driver.get("http://partner.tripleplay.in");
		driver.findElement(By.name("txtUserName")).sendKeys("appweb");
		driver.findElement(By.name("txtPassword")).sendKeys("appweb@123");
		driver.findElement(By.name("save")).click();
	}
}
