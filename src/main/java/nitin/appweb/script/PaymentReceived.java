package nitin.appweb.script;

import lombok.Data;

@Data
public class PaymentReceived {
	private String sr;
	private String recptNo;
	private String payMethod;
	private String date;
	private String amount;	
}
