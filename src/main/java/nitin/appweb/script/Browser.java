package nitin.appweb.script;

import java.util.Arrays;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Browser {

	public WebDriver callBrowser() {
		DesiredCapabilities capability = null;
		WebDriver driver = null;

		capability = DesiredCapabilities.chrome();
		capability.setBrowserName("chrome");
		capability.setPlatform(org.openqa.selenium.Platform.WINDOWS);
		capability.setCapability("chrome.switches", Arrays.asList("--disable-extensions"));
		System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");

		// To remove message "You are using an unsupported command-line
		// flag: --ignore-certificate-errors. Stability and security
		// will suffer." Add an argument 'test-type'
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");
		options.addArguments("test-type");
		options.addArguments("--disable-extensions");
		options.setExperimentalOption("excludeSwitches", Arrays.asList("enable-automation"));
		capability.setCapability(ChromeOptions.CAPABILITY, options);
		capability.setCapability(CapabilityType.SUPPORTS_ALERTS, true);
		driver = new ChromeDriver(capability);
		return driver;

	}
}
