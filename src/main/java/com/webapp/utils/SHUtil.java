package com.webapp.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author shahzad 22-Mar-2018
 *
 */
public class SHUtil {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	/**
	 * Method to write String to a File
	 */
	public static void writeToFile(String stringToWrite, String fileName) throws IOException {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fileName);
			fileWriter.write(stringToWrite);
			System.out.println("file writing has been done.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileWriter != null) {
				fileWriter.close();
			}
		}
	}

	/**
	 * SIMPLE WAY TO GET STRING FROM FILE
	 */
	public static String getStringFromFile(String fileName) throws IOException {
		return new String(Files.readAllBytes(Paths.get(fileName)));
	}

	public static Date stringToDate(String date) throws ParseException {
		return sdf.parse(date);
	}

	public static String dateToString(Date date) throws ParseException {
		return sdf.format(date);
	}

	public static int stringToInt(String str) {
		return Integer.parseInt(str);
	}

	public static Long stringToLong(String str) {
		return Long.valueOf(str);
	}

	public static double stringToDouble(String str) {
		return Double.parseDouble(str);
	}

	public static Calendar stringDateToCalenderDate(String strDate) throws ParseException {
		Date date = sdf.parse(strDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	public static int getNumberOfDays(Date d1, Date d2) {
		long diff = d2.getTime() - d1.getTime();
		return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

}
