package com.webapp.app;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.webapp.utils.SHUtil;

/**
 * 
 * @author shahzad 22-Mar-2018
 *
 */
public class App {

	public static void main(String[] args) throws IOException, ParseException {
		 String stringFromFile = SHUtil.getStringFromFile("Output.json");
		 writeToFile(stringFromFile);
		
		 System.out.println(SHUtil.stringToDate("12/12/2016"));
		 System.out.println(SHUtil.dateToString(SHUtil.stringToDate("12/12/2016")));
		
		 String str = "5";
		 System.out.println(SHUtil.stringToInt(str));
		 System.out.println(SHUtil.stringToDouble(str));
		 System.out.println(SHUtil.stringToLong(str));
		
		 Calendar cal = SHUtil.stringDateToCalenderDate("12/12/2018");
		 System.out.println(cal.getTime());
		 System.out.println(cal.getWeekYear());

		String dateStart = "11/03/2014 09:29:58";
		String dateStop = "15/03/2014 09:00:43";

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		try {
			Date d1 = format.parse(dateStart);
			Date d2 = format.parse(dateStop);
			System.out.println(SHUtil.getNumberOfDays(d1, d2));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private static void writeToFile(String stringFromFile) throws IOException {
		SHUtil.writeToFile(stringFromFile, "abc.txt");
	}

}
